package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var firstNumber = 0
    private var secondNumber = 0
    private var operation = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        button1.setOnClickListener {
            addText(button1)
        }
        button2.setOnClickListener {
            addText(button2)
        }
        button3.setOnClickListener {
            addText(button3)
        }
        button4.setOnClickListener {
            addText(button4)
        }
        button5.setOnClickListener {
            addText(button5)
        }

        button6.setOnClickListener {
            addText(button6)
        }

        button7.setOnClickListener {
            addText(button7)
        }

        button8.setOnClickListener {
            addText(button8)
        }
        button9.setOnClickListener {
            addText(button9)
        }
        button0.setOnClickListener {
            addText(button0)
        }
        PointButton.setOnClickListener {

        }
        PlusButton.setOnClickListener {
            addOpeation(PlusButton)
        }
        MinusButton.setOnClickListener {
            addOpeation(MinusButton)
        }
        EqualButton.setOnClickListener {
            equal()
        }
        DeleteButton.setOnClickListener {
            delete()
        }
        MultiplyButton.setOnClickListener {
            addOpeation(MultiplyButton)
        }
        DivideButton.setOnClickListener {
            addOpeation(DivideButton)
        }


    }

    private fun addText(button: Button) {
        textView.text = textView.text.toString() + button.text.toString()
    }

    private fun addOpeation(button: Button) {
        operation = button.text.toString()
        firstNumber = textView.text.toString().toInt()
        textView.text = ""
    }

    private fun delete() {
        var text = textView.text.toString()
        if (text.isNotEmpty()) {
            text = text.substring(0, text.length - 1)
            textView.text = text
        }
    }

    private fun equal() {
        if (textView.text.isNotEmpty()) {
            secondNumber = textView.text.toString().toInt()
            var result = 0
            if (operation == "+") {
                result = firstNumber + secondNumber
            } else if (operation == "-") {
                result = firstNumber - secondNumber
            } else if (operation == "*") {
                result = firstNumber * secondNumber
            } else if (operation == "/") {
                result = firstNumber / secondNumber
            }
            textView.text = result.toString()
            firstNumber = result

        }
    }
    private fun point() {
        textView.text = textView.text.toString() + "."
    }
}
